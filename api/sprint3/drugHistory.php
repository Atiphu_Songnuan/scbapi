<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language", "docNo", "orderDate", "periodFlag")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        if ($post['periodFlag'] === "Last" || $post['periodFlag'] === "History") {
            $c_sql_his = new class_mysql();
            $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

            $whereCondition = "";
            if ($post['periodFlag'] === "Last" && $post['orderDate'] != "" && $post['docNo'] != "") {
                // $docNoSplit = explode("/", $post['docNo']);
                // $whereCondition = "= '$post[orderDate]' AND A.LANE='$docNoSplit[0]' AND A.DAILY_NO='$docNoSplit[1]'";

                $chkDateFormat = DateTime::createFromFormat("Y-m-d", $post['orderDate']);
                if ($chkDateFormat) {
                    $docNoSplit = explode("/", $post['docNo']);
                    $whereCondition = "= '$post[orderDate]' AND A.LANE='$docNoSplit[0]' AND A.DAILY_NO='$docNoSplit[1]'";
                } else {
                    $result = array(
                        "messageCode" => 20000,
                        "messageDescription" => "format วันที่ไม่ถูกต้อง",
                        "messageStatus" => "fail",
                    );
                }

            } else if ($post['periodFlag'] === "History" && $post['orderDate'] == "" && $post['docNo'] == "") {
                $whereCondition = "Between DATE_ADD(CURDATE(), INTERVAL -6 MONTH) AND CURDATE()";
            }

            // print_r($whereCondition);
            //Parameter incorrect
            if ($whereCondition != "") {
                $sql = "SELECT
                    A.DISP_DATE AS orderDate,
                    A.BEG_TIME AS orderTime,
                    'แพทย์โรงพยาบาลสงขลานครินทร์' AS doctor,
                    B.DRUG_CODE AS drugCode,
                    'https://his01.psu.ac.th/HosApp/dev/img/drugcode/2681/1.png' AS drugImage,
                    I.TRADENAME AS medicine,
                    C.DRUG_NAME AS commonMedicineNameEn,
                    C.Dthai_name AS commonMedicineNameTh,
                    D.Instruction AS drugInstruction,
                    D.Instruction_2 AS drugRecommendation,
                    B.DRUG_AMT AS drugAmount,
                    C.SIZEUNIT AS drugUnitType,
                    H.RDU_INDICATION_TEXT AS moreInformation,
                    D.DrugDescription AS drugAttribute,
                    '' AS drugStorage,
                    CONCAT(F.HOS_COMMENT_TEXT, ' ', G.RDU_COMMENT_TEXT) AS warning
                FROM
                    Disp_day as A
                LEFT JOIN Item_day as B on A.HN = B.HN
                    AND A.DISP_DATE = B.DISP_DATE
                    AND A.LANE = B.LANE
                    AND A.DAILY_NO = B.DAILY_NO
                LEFT JOIN Drug_dat as C on B.DRUG_CODE = C.DRUG_CODE
                LEFT JOIN LBDrugList as D on B.HN = D.HN
                    AND B.DISP_DATE = D.DISP_DATE
                    AND B.LANE = D.LANE
                    AND B.DAILY_NO = D.DAILYNO
                    AND B.DRUG_CODE = D.DRUGCODE
                LEFT JOIN D_DRUG_INFO as E on B.DRUG_CODE = E.DRUG_CODE
                LEFT JOIN C_HOS_COMMENT as F on E.HOS_COMMENT_ID = F.HOS_COMMENT_ID
                LEFT JOIN C_RDU_COMMENT as G on E.RDU_COMMENT_ID = G.RDU_COMMENT_ID
                LEFT JOIN C_RDU_INDICATION as H on E.RDU_INDICATION_ID = H.RDU_INDICATION_ID
                LEFT JOIN D_DrugCatalog as I on B.DRUG_CODE = I.HospDrugCode
                WHERE   A.HN = '$hn'
                    AND A.DISP_DATE $whereCondition
                    AND A.OLD_DNO <> 'DDDD'
                ORDER BY
                    A.DISP_DATE,
                    A.BEG_TIME";

                // print_r($sql);

                $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
                $c_sql_his->close_sql($connnect);

                if (isset($data)) {
                    $drugHistoryList = array();
                    if (count($data) != 0) {
                        //Set Phone Number list
                        // $testdata = array("074541728","0958567419");
                        // $allMedicalWelfares = array();
                        foreach ($data as $v) {
                            foreach ($v as $key => $value) {
                                if ($v[$key] == null) {
                                    $v[$key] = "";
                                }

                                if ($key === "drugInstruction" || $key === "drugRecommendation" || $key === "warning") {
                                    $v[$key] = trim(preg_replace('/\s\s+/', ' ', $v[$key]));
                                }
                            }
                            array_push($drugHistoryList, $v);

                        }

                        $result = array(
                            "messageCode" => 10000,
                            "messageDescription" => "",
                            "messageStatus" => "success",
                            "drugHistoryList" => $drugHistoryList,
                        );
                    } else {
                        $result = array(
                            "messageCode" => 20000,
                            "messageDescription" => "ไม่มีข้อมูลประวัติการได้รับยา",
                            "messageStatus" => "fail",
                            "drugHistoryList" => $drugHistoryList,
                        );
                    }
                } else {
                    $result = array(
                        "messageCode" => 20000,
                        "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                        "messageStatus" => "fail",
                    );
                }
            }
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}
