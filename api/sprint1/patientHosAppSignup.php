<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "username", "email"))) {

    //Connect Database for check duplicate username or password
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HOSAPP']);

    $setEncode = "SET names tis620";
    $c_sql_his->run_sql($setEncode);

    // $sql = "SELECT  rphone, pinitial, pname, psur
    //         FROM    Mydata.Medrec
    //         WHERE   hn = '$post[hospitalNumber]'
    //         LIMIT   1";

    //Assign data from POST Request
    $hn = $post['hospitalNumber'];
    $username = $post['username'];
    $email = $post['email'];

    //Check already user
    $sql = "SELECT  hn
                    FROM    HOSAPP.AppUser
                    WHERE   hn='$hn'
                    OR      user = '$username'
                    OR      email = '$email'
                    LIMIT   1";
    $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);

    if (isset($data['hn'])) {
        // print_r("มีข้อมูลการลงทะเบียนแล้ว");
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "username or email already exist",
            "messageStatus" => "fail",
        );
    } else {
        //Connect Database For insert new Data
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HOSAPP']);
        $newUser = "INSERT INTO HOSAPP.AppUser (hn, user, email, regis_date)
                    VALUES ('$hn', '$username', '$email', now())";

        $c_sql_his->run_sql($newUser);

        // Recheck for insert success
        $checkSql = "SELECT  hn
                    FROM    HOSAPP.AppUser
                    WHERE   hn='$hn'";
        $reCheckData = $c_sql_his->run_sql_return_array1d($checkSql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        if (isset($reCheckData)) {
            // print_r("เพิ่มข้อมูลสำเร็จ");
            $result = array(
                "messageCode" => 10000,
                "messageDescription" => "signup success",
                "messageStatus" => "success",
            );
        } else {
            // print_r("ไม่สามารถเพิ่มข้อมูลได้");
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "signup failed",
                "messageStatus" => "fail",
            );
        }
    }
}
