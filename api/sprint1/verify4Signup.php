<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

// print_r("IP: ". $c_sry->getip());
// $entityBody = file_get_contents('php://input');
// $rawData = json_decode( file_get_contents( 'php://input' ), true );
if ($c_fun->is_body($post, array("hospitalNumber", "typeOfID", "idNumber", "dateOfBirth", "firstName", "lastName", "fromSystem", "language"))) {

    $checkHN = $post['hospitalNumber'];
    $hn = $c_fun->check_hospital_number_length($checkHN);

    if (isset($hn)) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        $sql = "SELECT  rphone, pinitial, pname, psur
            FROM    Medrec
            WHERE   hn = '$hn'
            AND     pphone ='$post[idNumber]'
            AND     date(bdate) ='$post[dateOfBirth]'
            AND     pname = '$post[firstName]'
            AND     psur = '$post[lastName]'
            LIMIT   1";

        $patientData = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);

        $c_sql_his->close_sql($connnect);

        if (isset($patientData['pname'])) {

            //********** For Test **********//
            $phoneNumber = "";
            // 986870
            switch ($hn) {
                //พี่จรูญ
                case '0706025':
                    $phoneNumber = "0969045350";
                    break;
                //พี่ฝรั่ง
                case '986870':
                    $phoneNumber = "0899777167";
                    break;
                //แฟ้ม
                case '0454728':
                    $phoneNumber = "0958567419";
                    break;
                //พี่พล
                case '0398988':
                    $phoneNumber = "0958567419";
                    break;
                //พี่ตั้ม
                case '1968556':
                    $phoneNumber = "0954309380";
                    break;
                //กิต
                case '0668345':
                    $phoneNumber = "0877579590";
                    break;
                //นัท
                case '1892219':
                    $phoneNumber = "0805258215";

                //พี่บอย
                case '1207120':
                    $phoneNumber = "0958567419";
                    break;
                    
                default:
                    $phoneNumber = preg_replace('/[^0-9]/', '', $patientData['rphone']);
                    break;
            }

            if ((strlen($phoneNumber) == 10) && ($patientData['rphone'][0] === "0")) {
                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "Sign up success",
                    "messageStatus" => "success",
                    "mobileNo" => $phoneNumber,
                    "titleName" => $patientData['pinitial'],
                    "firstName" => $patientData['pname'],
                    "lastName" => $patientData['psur'],
                    "middleName" => "",
                    "titleNameEn" => "",
                    "middleNameEn" => "",
                    "firstNameEn" => "",
                    "lastNameEn" => "",
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "เบอร์โทรศัพท์ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อแก้ไขเบอร์โทรศัพท์",
                    "messageStatus" => "fail",
                );
            }
            //**************************************************** */

            // $phoneNumber = preg_replace('/[^0-9]/', '', $patientData['rphone']);
            // if ((strlen($phoneNumber) == 10) && ($patientData['rphone'][0] === "0")) {
            //     $result = array(
            //         "messageCode" => 10000,
            //         "messageDescription" => "Sign up success",
            //         "messageStatus" => "success",
            //         "mobileNo" => $phoneNumber,
            //         "titleName" => $patientData['pinitial'],
            //         "firstName" => $patientData['pname'],
            //         "lastName" => $patientData['psur'],
            //         "middleName" => "",
            //         "titleNameEn" => "",
            //         "middleNameEn" => "",
            //         "firstNameEn" => "",
            //         "lastNameEn" => "",
            //     );
            // } else {
            //     $result = array(
            //         "messageCode" => 20000,
            //         "messageDescription" => "เบอร์โทรศัพท์ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อแก้ไขเบอร์โทรศัพท์",
            //         "messageStatus" => "fail",
            //     );
            // }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}
