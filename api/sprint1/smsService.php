<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

// print_r("IP: ". $c_sry->getip());
if ($c_fun->is_body($post, array("sms", "mobileNo", "hospitalNumber", "fromSystem"))) {
    //*** Check เลขตัวแรกในเบอร์โทรศัพท์ว่าเป็น 0 หรือไม่*/
    //* ถ้าเป็น 0 ให้เปลี่ยนเป็นเลข 66 (Thai Country code) */
    //***กรณีเบอร์โทรศัพท์ขึ้นต้นด้วย 0 */

    if (strlen($post['mobileNo']) == 10) {
        if ($post['mobileNo'][0] === "0") {
            $countryPhoneNumber = "66" . substr($post['mobileNo'], 1);
            $res = sms_sending($countryPhoneNumber, $post['sms']);
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "Send SMS failed",
                "messageStatus" => "fail",
            );
        }
    }
    
    // //***กรณีเบอร์โทรศัพท์ขึ้นต้นด้วย 66 */
    // else if (strlen($post['mobileNo']) == 11) {
    //     $countryPhoneNumber = $post['mobileNo'][0] . $post['mobileNo'][1];
    //     if ($countryPhoneNumber === "66") {
    //         $res = sms_sending($post['mobileNo'], $post['sms']);
    //     } else {
    //         $result = array(
    //             "messageCode" => 20000,
    //             "messageDescription" => "Send SMS failed",
    //             "messageStatus" => "fail",
    //         );
    //     }
    // }

    $resSplit = explode("\n", $res);
    $queueSplit = explode(" ", $resSplit[1]);
    $queue = $queueSplit[1];
    // print_r("Queued: " . $queue . "\n");

    // $queue = "M192171412016298";
    if (isset($queue)) {
        sleep(3);
        $sendStatus = sms_status($queue);

        // print_r($sendStatus);

        $statusSplit = explode("\t", $sendStatus);
        $date = str_replace("/", "-", $statusSplit[0]);
        $statusCode = $statusSplit[2];

        // print_r($statusCode);

        switch ($statusCode) {
            case "Y":
                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "Send SMS success",
                    "messageStatus" => "success",
                );
                break;
            case "F":
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "Send SMS failed",
                    "messageStatus" => "fail",
                );
                break;
            case "D":
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "SMS deleted from server",
                    "messageStatus" => "fail",
                );
                break;
            // case "R":
            //     $result = array(
            //         "messageCode" => 21006,
            //         "messageDescription" => "Recieved delivery report from recipient mobile phone",
            //         "messageStatus" => "fail",
            //     );
            //     break;
            default:
                break;
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "Send SMS failed",
            "messageStatus" => "fail",
        );
    }
}

function sms_sending($tar_num, $tar_msg)
{
    // echo $tar_num."\n";
    // echo $tar_msg."\n";
    // echo $mode."\n";

    // $api = "http://172.29.70.202/cmd/system/api/sendsms.cgi";
    // $sendApi = "http://192.168.40.202/cmd/system/api/sendsms.cgi?tar_num=$tar_num&tar_msg=$tar_msg&tar_mode=$mode";
    $sendApi = "http://192.168.40.202/cmd/system/api/sendsms.cgi";
    $sendBody = array(
        'tar_num' => $tar_num,
        'tar_msg' => $tar_msg,
        'tar_mode' => "text",
    );

    $sendCtx = array("http" => array(
        "method" => "POST",
        "header" => "Content-type: application/x-www-form-urlencoded",
        "content" => http_build_query($sendBody),
    ));

    $sendContext = stream_context_create($sendCtx);
    $sendRes = @file_get_contents($sendApi, false, $sendContext);

    // print_r($sendApi);
    // $sendRes = @file_get_contents($sendApi);
    return $sendRes;
}

function sms_status($msgid)
{
    // $api = "http://172.29.70.202/cmd/system/api/msgstatus.cgi";
    // $api = "http://192.168.40.202/cmd/system/api/msgstatus.cgi?mode=queue&msgid=$msgid";
    // $res = @file_get_contents($api);

    $api = "http://192.168.40.202/cmd/system/api/msgstatus.cgi";
    $body = array(
        'mode' => "queue",
        'msgid' => $msgid,
    );

    $ctx = array("http" => array(
        "method" => "POST",
        "header" => "Content-type: application/x-www-form-urlencoded",
        "content" => http_build_query($body),
    ));

    $context = stream_context_create($ctx);

    $res = @file_get_contents($api, false, $context);
    return $res;
}
