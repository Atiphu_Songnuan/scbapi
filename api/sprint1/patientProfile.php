<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        $ename = explode(" ", GetEName($c_sql_his, $hn));
        $blood = GetBloodGroup($c_sql_his, $hn);
        $address = GetAddress($c_sql_his, $hn);

        $sql = "SELECT medrec.hn,
                                medrec.pinitial ,
                                medrec.pname,
                                medrec.psur,
                                '" . $ename[0] . "' as titleNameEn,
                                '" . $ename[1] . "' as firstNameEn,
                                '" . $ename[2] . "' as lastNameEn,
                                IF (sex='1','M','F') as gender,
                                date(medrec.bdate) AS dateOfBirth,
                                medrec.rphone,
                                '" . $blood . "' as bloodGroup,
                                concat(medrec.paddress, '" . $address . "') as address,
                                rname as contactPersonName
                    FROM        Medrec as medrec
                    LEFT JOIN   BB_Patient as blood
                    ON          medrec.hn = blood.hn
                    WHERE       medrec.hn='$hn'
                    LIMIT       1";

        // $sql = "SELECT * FROM Medrec WHERE HN = '0454728'";

        $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        foreach ($data as $k => $v) {
            if (!isset($data[$k])) {
                $data[$k] = "";
            }
        }

        if (count($data) != 0) {
            //Set Phone Number list
            // $testdata = array("074541728","0958567419");
            $allPhoneNumber = array($data['rphone']);
            $phoneNumberList = array();
            foreach ($allPhoneNumber as $v) {
                array_push($phoneNumberList,
                    array(
                        "phoneType" => "",
                        "phoneNumber" => $v,
                    )
                );
            }
            //************************************ */

            //Set Contact Person phone number list
            $allContactPersonPhoneNumberList = array();
            $contactPersonPhoneNumberList = array(
                "contactPersonPhoneNumberType" => "",
                "contactPersonPhoneNumber" => "",
            );
            array_push($allContactPersonPhoneNumberList, $contactPersonPhoneNumberList);
            //************************************ */

            //Set Contact Person list
            $allContactPersonList = array();
            $contactPersonList = array(
                "contactPersonName" => $data['contactPersonName'],
                "contactPersonRelation" => "",
                "contactPersonPhoneNumberList" => $allContactPersonPhoneNumberList,
            );
            array_push($allContactPersonList, $contactPersonList);
            //************************************ */

            $result = array(
                "messageCode" => 10000,
                "messageDescription" => "Sign up success",
                "messageStatus" => "success",
                "patientCode" => "",
                "titleName" => $data['pinitial'],
                "firstName" => $data['pname'],
                "lastName" => $data['psur'],
                "middleName" => "",
                "titleNameEn" => $data['titleNameEn'],
                "firstNameEn" => $data['firstNameEn'],
                "lastNameEn" => $data['lastNameEn'],
                "middleNameEn" => "",
                "gender" => $data['gender'],
                "dateOfBirth" => $data['dateOfBirth'],
                "bloodGroup" => $data['bloodGroup'],
                "address" => $data['address'],
                "phoneNumberList" => $phoneNumberList,
                "contactPersonList" => $allContactPersonList,
                "profileImage" => "",
            );
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }

    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

function GetEname($c_sql_his, $hospitalNumber)
{
    $sql = "SELECT replace(name,'   ',' ') AS fullname FROM Ename WHERE hn='" . $hospitalNumber . "'";
    $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
    return $data['fullname'];
}

function GetBloodGroup($c_sql_his, $hospitalNumber)
{
    $sql = "SELECT concat(BGrp,RH) AS BloodGroup FROM BB_Patient WHERE hn='" . $hospitalNumber . "'";
    $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
    if ($data['BloodGroup'] == null) {
        $data['BloodGroup'] = "";
    }
    $blood = $data['BloodGroup'];
    return $blood;
}

function GetAddress($c_sql_his, $hospitalNumber)
{
    $sql = "SELECT parea FROM Medrec WHERE hn='" . $hospitalNumber . "'";
    $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
    if ($data['parea'] != null) {
        if (strlen($data['parea']) == 6) {
            $cwat = substr($data['parea'], 0, 2);
            $cphur = substr($data['parea'], 2, 2);
            $cbon = substr($data['parea'], 4, 2);

        } else if (strlen($data['parea']) == 4) {
            $cwat = substr($data['parea'], 0, 2);
            $cphur = substr($data['parea'], 2, 2);
            $cbon = "";

        }
    }
    $address = GetFullAddress($c_sql_his, $cwat, $cphur, $cbon);
    return $address;
}

function GetFullAddress($c_sql_his, $cwat, $cphur, $cbon)
{
    if ($cbon != null || $cbon != "") {
        $sql = "SELECT concat(' ', nbon,' ',nphur,' ',nwat) AS address
                FROM Fwat as A
                LEFT JOIN Famphur as B on A.cwat=B.cwat
                LEFT JOIN Ftumbon as C on C.cwatphur=concat(A.cwat,B.cphur)
                WHERE  B.cwat='" . $cwat . "'
                AND B.cphur='" . $cphur . "'
                AND C.cbon='" . $cbon . "'";
    } else {
        $sql = "SELECT concat(' ', nbon,' ',nphur,' ',nwat) AS address
                FROM Fwat as A
                LEFT JOIN Famphur as B on A.cwat=B.cwat
                LEFT JOIN Ftumbon as C on C.cwatphur=concat(A.cwat,B.cphur)
                WHERE  B.cwat='" . $cwat . "'
                AND B.cphur='" . $cphur . "'";
    }
    $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
    if ($data['address'] == null) {
        $data['address'] = "";
    }
    // print_r($data['address']);
    $splitAddress = explode(" ", $data['address']);
    // $address = " ต." . $splitAddress[1] . " อ." . $splitAddress[2] . " จ." . $splitAddress[3];
    $address = $data['address'];
    return $address;
}
