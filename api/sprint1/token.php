<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("grantType", "clientId", "hospitalNumber"))) {

    if($post['clientId'] != $CONFIGS['clientId']){
        $result = array(
            "messageCode" => 14000,
            "messageDescription" => "clientId not valid",
            "messageStatus" => "fail",
        );
    }else{
        $hn = $post['hospitalNumber'];
        $expiresIn = (60 * 60) * (24 * 30) * 6; // 6 months
        $expired = time() + $expiresIn;

        $payload = array(
            "hn" => $hn,
            "exp" => $expired
        );

        $result = array(
            "messageCode" => 10000,
            "messageDescription" => "access token success",
            "messageStatus" => "success",
            "accessToken" => $c_sry->jwt->encode(json_encode($payload)),
            "tokenType" => "Bearer",
            "expiresIn" => ($expiresIn / 60),
        );

        $is_token = true;
    }

}



    /*$expiresIn = (60 * 60) * (24 * 30) * 6; // 6 months
    $expired = time() + $expiresIn;

    $list = array(
        "0053027" => "3909900013359",
        "0398988" => "1909800459979",
        "0400880" => "3909900377431",
        "0447324" => "1909800549650",
        "0454728" => "1909800565299",
        "0486591" => "3919900030598",
        "0511587" => "3901000441071",
        "0601262" => "3909800274363",
        "0607277" => "3909801145885",
        "0645791" => "4901100001829",
        "0660365" => "3901100693846",
        "0668345" => "2439900017890",
        "0691725" => "3909900681807",
        "0692899" => "3909800761868",
        "0711336" => "3810100710945",
        "0842070" => "3900300545505",
        "0927950" => "3801600610762",
        "0941085" => "3810200192431",
        "0986870" => "3800400011791",
        "1004927" => "3101201299920",
        "1116351" => "3969900185800",
        "1116436" => "3901000217627",
        "1168047" => "5900199011764",
        "1207120" => "3901100279799",
        "1249657" => "1909900027493",
        "1273226" => "3801200699308",
        "1424829" => "3919900155111",
        "1488237" => "1959900104005",
        "1514014" => "1909800064524",
        "1582006" => "1910600003663",
        "1892219" => "1820500124478",
        "1968556" => "1909800307176",
        "1991478" => "2939900010238"
    );

    foreach ($list as $key => $value) {
        $payload = array(
            "hn" => $key,
            "exp" => $expired
        );
        echo "$key\t$value\t";
        echo $c_sry->jwt->encode(json_encode($payload));
        echo "\n";
    }
    die;*/