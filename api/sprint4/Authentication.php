<?php
// WS008-1-Authentication(KIOSK)
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if (is_input($post, array("type"))) {

    $dbname = $isProduction? 'Mydata':'DATATEST';
    $sql = "SELECT      medrec.hn,
                        medrec.pinitial ,
                        medrec.pname,
                        medrec.psur,
                        date(medrec.bdate) AS dateOfBirth,
                        (YEAR(NOW()) - YEAR(medrec.bdate)) as age,
                        medrec.rphone,
                        concat(blood.BGrp, blood.RH) as bloodGroup,
                        concat(medrec.paddress, ' ', medrec.rarea) as address
            FROM        $dbname.Medrec as medrec
            LEFT JOIN   $dbname.BB_Patient as blood ON medrec.hn = blood.hn 
            WHERE ";
    
    switch ($post['type']) {

        case 'CITIZENID':
            $citizenId = $post['citizenId'];
            $sql.= "medrec.pphone = '$citizenId' ";
            break;

        case 'INVOICEID':
            $his2 = new class_mysql();
            $connnect2 = $his2->connectSQL($CONFIGS['server']['HOSAPP']);
            $dbname = $isProduction? 'PAYMENT':'PAYMENT_BETA';
            $invoiceId = $post['invoiceId'];
            $sql2 = "SELECT hospitalNumber FROM $dbname.invoice WHERE invoiceId = '$invoiceId' LIMIT 1";
            $data = $his2->run_sql_return_array1d($sql2);
            $his->close_sql($connnect2);
            $hn = $data[0];
            $sql.= "medrec.hn = '$hn' ";
            break;

        default:
            //HOSPITALNUMBER
            $hn = $post['hospitalNumber'];
            $sql.= "medrec.hn = '$hn' ";
            break;
    }

    $sql.= "LIMIT 1";
    
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HIS']);
    $data = $his->run_sql_return_array1d($sql);
    $his->close_sql($connnect);

    $name =  $data['pname'].' '.$data['psur'];
    $result = array(
        'hospitalNumber' => $data['hn'],
        'nameTh' =>  $name,
        'nameEn' =>  $name,
        'age' => intval($data['age']),
    );

}

function is_input($input, $key){
    foreach ($key as $v) {
        if (!isset($input[$v]))  return false;
    }
    return true;
}


