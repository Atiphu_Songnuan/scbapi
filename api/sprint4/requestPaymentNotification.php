<?php
//WS007-1-Receive Payment Noti
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if (is_input($post, array("invoice", "billList"))) {

    $result = InsertInvoice();

}

function InsertInvoice(){
    global $CONFIGS, $isProduction;

    $rfu = new restful();
    if($rfu->getMethod() != 'POST') $rfu->return_status(400, true);

    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $dbname = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    $data = @file_get_contents('php://input');
    $data = json_decode($data, true);
    $invoiceId = $data['invoice']['invoiceId'];

    $sql = "SELECT COUNT(id) FROM $dbname.invoice WHERE invoiceId = '$invoiceId' ";
    $check = $his->run_sql_return_array1d($sql);

    if(intval($check[0]) > 0){
        return array(
            'status' => false,
            'error' => 'มีเลข Invoice นี้อยู่ในระบบแล้ว',
        );
    }

    $his->run_sql("BEGIN");

    $data['invoice']['payable'] = $data['invoice']['payable']? '1':'0';
    $sql = $rfu->toSqlQuery(array(
        'method' => 'PUT',
        'data' => $data['invoice'],
        'table' => $dbname.'.invoice',
    ));
    
    $his->run_sql($sql);
    $id = $his->insert_id();
    $countList = count($data['billList']);
    
    foreach ($data['billList'] as $v) {
        $v['invoice_id'] = $id;
        $v['performStatus'] = $v['performStatus']? '1':'0';
        $sql = $rfu->toSqlQuery(array(
            'method' => 'PUT',
            'data' => $v,
            'table' => $dbname.'.bill',
        ));
        $his->run_sql($sql);
        $totalAmount += floatval($v['amount']);
    }

    $sql = "SELECT COUNT(id) FROM $dbname.invoice WHERE invoiceId = '$invoiceId' ";
    $check = $his->run_sql_return_array1d($sql);

    $sql = "SELECT COUNT(id) FROM $dbname.bill WHERE invoice_id = '$id' ";
    $check2 = $his->run_sql_return_array1d($sql);

    if(intval($check[0]) == 1 && intval($check2[0]) == $countList){
        $his->run_sql("COMMIT");
    }else{
        $his->run_sql("ROLLBACK");
    }

    $access_token = "";
    $isProduction = true;
    if($isProduction){
        $ctoken = new accessToken($his);
        $data = $ctoken->get();
        $access_token = $data['accessToken'];
    }

    return array(
        'status' => empty($access_token)? false:true,
        'access_token' => $access_token
    );

    $his->close_sql($connnect);
}

function is_input($input, $key){
    foreach ($key as $v) {
        if (!isset($input[$v]))  return false;
    }
    return true;
}


