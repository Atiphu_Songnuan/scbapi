<?php
//WS007-6-UnLock Payment
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "station", "verifyRefID", "citizenId")) && isset($hn)) {

    $result = Unlock($hn, $post);

}

function Unlock($hn, $post){
    global $CONFIGS, $isProduction;

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    $verifyRefID = $post['verifyRefID'];
    $sql = "DELETE FROM $PAYMENT.lock WHERE verifyRefID = '$verifyRefID' ";
    $his->run_sql($sql);
    $his->close_sql($connnect);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
    );
}