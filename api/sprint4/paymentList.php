<?php
//WS007-2-Get Payment List
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if (is_input($post, array("hospitalNumber", "language")) && isset($hn)) {
    
    $result = PaymentList($hn, $post);

}

function PaymentList($hn, $post){
    global $CONFIGS, $isProduction, $c_fun;

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
            "invoiceList" => array(),
        );
    }

    $rfu = new restful();
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $dbname = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    $citizenId = intval($post['citizenId']);
    $sql = "SELECT *
            FROM $dbname.invoice AS A 
            WHERE (A.hospitalNumber = '$hn' || A.citizenId = '$citizenId') && A.invoiceDateTime >= (NOW() - INTERVAL 24 HOUR)
            ORDER BY A.invoiceDateTime DESC
            LIMIT 100";
    $data = $his->run_sql_return_array($sql);

    $list = array();
    foreach ($data as $k => $v) {
        $sql = "SELECT SUM(B.amountOverRight) AS totalAmount
                FROM $dbname.bill AS B 
                WHERE B.invoice_id = '$v[id]' ";
        $totalAmount = $his->run_sql_return_array1d($sql);
        $list[] = array(
            'invoiceId' => $v['invoiceId'],
            'invoiceDateTime' => $c_fun->echoDatetime($v['invoiceDateTime']),
            'performDate' => $c_fun->echoDatetime($v['performDate']),
            'totalAmount' => $totalAmount['totalAmount'],
            'payable' => ($v['payable']==1 ? true:false),
            'remark' => $v['remark'],
            'doctor' => $v['doctor'],
            'clinic' => $v['clinic'],
            'serviceDeliveryLocationCode' => $v['serviceDeliveryLocationCode'],
            'serviceDeliveryLocationName' => $v['serviceDeliveryLocationName']
        );
    }
    $his->close_sql($connnect);

    if(empty($list)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "ไม่มีข้อมูลรายการที่ต้องชำระ",
                "The invoices already have been paid",
            ),
            "messageStatus" => "fail",
            "invoiceList" => $list,
        );
    }else{
        return array(
            "messageCode" => 10000,
            "messageDescription" => "",
            "messageStatus" => "success",
            "invoiceList" => $list,
        );
    }

}

function is_input($input, $key){
    foreach ($key as $v) {
        if (!isset($input[$v]))  return false;
    }
    return true;
}