<?php
//http://203.154.116.83/ws/rest/receiveMasterDataClinicCode
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
$c_sql_his = new class_mysql();
$connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

// $setEncode = "SET names tis620";
// $c_sql_his->run_sql($setEncode);

$sql = "SELECT HN,CODE_B FROM Opdq WHERE  DATE_D=curdate()  AND (isnull(OUTTIME) OR OUTTIME = '') ORDER BY HN LIMIT 15";

$data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);

$queueList = array();
$queueData = array();

// $result = array(
//     "messageCode" => 10000,
//     "messageDescription" => "",
//     "messageStatus" => "success",
// );
// print_r($data);

if (isset($data)) {
    foreach ($data as $k => $v) {
        $lastopd= $data[$k]["CODE_B"];
        $iHN = $data[$k]["HN"];
        
        $sql2 = "SELECT CODE_B,
                        C_DOCT,
                        A.HN,
                        DATE_Q,
                        QTIME,
                        DOCTTIME,
                        OUTTIME,
                        QUEUE,
                        PTAPP,
                        PNAME
                FROM Opdq as A 
                LEFT JOIN Medrec as B on A.HN=B.HN 
                WHERE A.HN='$iHN' 
                    AND CODE_B='$lastopd'  
                    AND DATE_D=curdate()  
                    AND (isnull(OUTTIME) OR OUTTIME = '')
                ORDER BY QTIME";

        $data2 = $c_sql_his->run_sql_return_array($sql2, MYSQLI_ASSOC);

        if (isset($data2)) {
            // print_r($data2);
            foreach ($data2 as $k2 => $v2) {
                $cDoct = $data2[$k2]["C_DOCT"];
                $qTime = $data2[$k2]["QTIME"];
                $codeB = $data2[$k2]["CODE_B"];
                $queue = $data2[$k2]["QUEUE"];

                //หาจำนวนคิวของแพทย์คนนั้นๆ
                $sql3="SELECT count(HN) AS DOCCOUNT
                    FROM Opdq 
                    WHERE DATE_D>=curdate() 
                        AND C_DOCT= '$cDoct' 
                        AND (isnull(DOCTTIME) OR DOCTTIME = '')
                        AND QTIME < '$qTime' 
                        AND QTIME not LIKE '%1' 
                        AND CODE_B='$lastopd'";

                $data3 = $c_sql_his->run_sql_return_array($sql3, MYSQLI_ASSOC);

                // print_r($data2[$k2]["HN"]."\n");
                // print_r($cDoct."\n");
                // print_r($qTime."\n");
                // print_r($data3);

                if (isset($data3)) {

                    foreach ($data3 as $k3 => $v3) {
                        // print_r($qTime."\n");
                        // print_r($codeB."\n");
                        // print_r($cDoct."\n");
                        // print_r($lastopd."\n");


                        $sql4="SELECT count(hn) AS QCOUNT
                        FROM Appoint 
                        WHERE date_ap=curdate() 
                        AND time_ap < '$qTime' 
                        AND code_ap='$codeB' 
                        AND c_doct='$cDoct' 
                        AND hn not in (
                            SELECT hn 
                            FROM Opdq
							where date_d>=curdate()   
                            AND c_doct='$cDoct'  
                            AND code_b='$lastopd'
                        )";

                        $data4 = $c_sql_his->run_sql_return_array($sql4, MYSQLI_ASSOC);
                        
                        $queueCount;
                        if (isset($data4)) {
                            foreach ($data4 as $k4 => $v4) {
                                $queueCount = $data4[$k4]["QCOUNT"];
                            }

                            $nq = $data3[$k3]["DOCCOUNT"] + 2 + $queueCount;
                            // print_r("Queue Count: " . $nq . "\n");

                            if ($nq > 0) {
                                $sql5 = "SELECT u_name FROM Funit WHERE c_unit='$codeB'";
                                $data5 = $c_sql_his->run_sql_return_array($sql5, MYSQLI_ASSOC);
                                
                                $location = $data5[0]["u_name"];

                                $sql6 = "SELECT name FROM Medperson WHERE perid='$cDoct'";
                                $data6 = $c_sql_his->run_sql_return_array($sql6, MYSQLI_ASSOC);

                                $doctor = $data6[0]["name"];
                                
                                $queueData = array(
                                    "messageCode" =>"0",
                                    "messageDescription" =>"",
                                    "messageStatus" =>"success",
                                    "hospitalNumber" => $iHN,
                                    "language" => "TH",
                                    "appointmentId" => "-",
                                    "appointmentTime" => $qTime,
                                    "queueSystem" => "คิวพบแพทย์",
                                    "queueNo" => $queue,
                                    "currentQueueNo" =>"",
                                    "amountQueueForWaiting" => $nq,
                                    "location" => $location,
                                    "moreInformation" =>"",
                                    "remark" => "",
                                    "doctor" => $doctor,
                                    "notificationType" => ""
                                );

                                // print_r($queueData);
                                array_push($queueList, $queueData);
                            }
                        }
                    }
                }
            }
        }
    }
    $c_sql_his->close_sql($connnect);

    $result = $queueList;

    // $scriptFile = __DIR__ . "/sendQueueNotification.py";
    // sleep(3);

    // $command = escapeshellcmd("python sendQueueNotification.py " .$jsonFile);
    print_r($queueList);
    
} else {
    $result = array(
        "messageCode" => 20000,
        "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
        "messageStatus" => "fail",
    );
}