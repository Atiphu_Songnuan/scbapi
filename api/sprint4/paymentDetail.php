<?php
//WS007-3-Get Payment Detail
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language", "invoiceId", "station", "citizenId")) && isset($hn)) {

    $result = paymentDetail($post,$hn);

}
 
function paymentDetail($post, $hn){
    global $CONFIGS, $isProduction, $c_fun;

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $rfu = new restful();
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $dbname = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    $invoiceId = $post['invoiceId'];
    $station = $post['station'];
    $citizenId = intval($post['citizenId']);

    if($station != 'HosApp' && $station != 'Kiosk'){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "จุดชำระเงินไม่ถูกต้อง",
                "Invalid station"
            ),
            "messageStatus" => "fail",
        );
    }


    //invoice
    $sql = $rfu->toSqlQuery(array(
        'method' => 'GET',
        'query' => '/invoiceId/'.$invoiceId.'/?hospitalNumber='.$hn.'&&citizenId='.$citizenId,
        'table' => $dbname.'.invoice',
        'limit' => 1
    ));
    $invoice = $his->run_sql_return_array1d($sql);

    if(empty($invoice)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "ไม่พบหมายเลขใบแจ้งยอดชำระในระบบ",
                "Error invoiceId not found"
            ),
            "messageStatus" => "fail",
        );
    }

    //bill
    $sql = $rfu->toSqlQuery(array(
        'method' => 'GET',
        'query' => '/invoice_id/'.$invoice['id'].'/?sort=id_asc',
        'table' => $dbname.'.bill',
        'limit' => 100
    ));
    $bills = $his->run_sql_return_array($sql);
    $billList = array();
    $seq = 1;
    $totalAmountInRight = 0;
    $totalAmountOverRight = 0;
    $totalAmount = 0;
    foreach ($bills as $v) {
        $billList[] = array(
            "billSequenceNo" => "$seq",
            "billName" => $v['billName'],
            "amountInRight" => $v['amountInRight'],
            "amountOverRight" => $v['amountOverRight'],
            "performStatus" => ($v['performStatus']==1)? true:false,          
        );
        $seq++;
        $totalAmountInRight += floatval($v['amountInRight']);
        $totalAmountOverRight += floatval($v['amountOverRight']);
        $totalAmount += floatval($v['amountOverRight']);
    }
    $his->close_sql($connnect);

    //info user
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HIS']);
    $Mydata = $isProduction? 'Mydata':'DATATEST';
    $sql = "SELECT A.pname, A.psur
            FROM $Mydata.Medrec as A
            WHERE A.hn ='$hn'
            LIMIT 1";
    $name = $his->run_sql_return_array1d($sql);
    $his->close_sql($connnect);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "hospitalNumber" => "$hn",
        "name" => "$name[pname] $name[psur]",
        "performDate" => $c_fun->echoDatetime($invoice['performDate']),
        "invoiceId" => $invoice['invoiceId'],
        "invoiceDateTime" => $c_fun->echoDatetime($invoice['invoiceDateTime']),
        "eligible" => $invoice['eligible'],
        "billList" => $billList,
        "totalAmountInRight" => $totalAmountInRight,
        "totalAmountOverRight" => $totalAmountOverRight,
        "totalAmount" => $totalAmount
    );

}


