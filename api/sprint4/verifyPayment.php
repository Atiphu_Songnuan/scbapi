<?php
//WS007-4-Verify Payment
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "invoiceList", "totalAmount", "stationID")) && isset($hn)) {

    $result = Verify($hn, $post);

}

function Verify($hn, $post){
    global $CONFIGS, $isProduction;

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $invoiceList = $post['invoiceList'];
    $totalAmount = $post['totalAmount'];
    $stationID = substr($post['stationID'], 0, 3);

    //check OPD.Allmoney
    if($isProduction){
        $his = new class_mysql();
        $connnect = $his->connectSQL($CONFIGS['server']['HIS']);
        $OPD = $isProduction? 'OPD':'DATATEST';
        $total = 0;
        foreach ($invoiceList as $v) {
            if(isset($v['invoiceId'])){
                $InvoiceId = $v['invoiceId'];
                $sql = "SELECT SUM(MONEY)
                        FROM $OPD.Allmoney
                        WHERE HN = '$hn'
                        AND InvoiceId = '$InvoiceId'
                        AND (PAY != 'Y' || isnull(Pay))
                        AND (old_ref != 'DDDD' || isnull(old_ref))";
                $Allmoney = $his->run_sql_return_array1d($sql);
                $amount = $Allmoney[0];
                if($amount == 0){
                    return array(
                        "messageCode" => 20000,
                        "messageDescription" => array(
                            "ใบแจ้งยอดชำระ $invoiceId ได้รับการชำระเงินแล้ว กรุณาตรวจสอบ",
                            "Invoice $invoiceId has been paid"
                        ),
                        "messageStatus" => "fail",
                    );
                }
                $total += $amount;
            }
        }
        $his->close_sql($connnect);
        if($total != $totalAmount){
            return array(
                "messageCode" => 20000,
                "messageDescription" => array(
                    "จำนวนเงินรวมของทุกใบแจ้งยอดชำระไม่ตรงกัน กรุณาทำรายการใหม่อีกครั้ง",
                    "Error, the total amount of Invoices"
                ),
                "messageStatus" => "fail",
            );
        }
    }

    //check PAYMENT.invoice
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $total = 0;
    $roundOff = 0;
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    $citizenId = "";
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){
            $invoiceId = $v['invoiceId'];
            $sql = "SELECT id, citizenId
                    FROM $PAYMENT.invoice
                    WHERE invoiceId = '$invoiceId' && hospitalNumber = '$hn'
                    LIMIT 1";
            $invoice = $his->run_sql_return_array1d($sql);
            if(empty($invoice)){
                return array(
                    "messageCode" => 20000,
                    "messageDescription" => array(
                        "ใบแจ้งยอดชำระ $invoiceId ไม่มีอยู่ในระบบ",
                        "Error, invoice $invoiceId not found"
                    ),
                    "messageStatus" => "fail"
                );
            }
            if(empty($citizenId)) $citizenId = $invoice['citizenId'];
            $id = $invoice['id'];
            $sql = "SELECT SUM(amountOverRight), SUM(roundOff)
                    FROM $PAYMENT.bill
                    WHERE invoice_id = '$id'";
            $bill = $his->run_sql_return_array1d($sql);
            $total += floatval($bill[0]);
            $roundOff += floatval($bill[1]);
        }
    }
    if($total != $totalAmount){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "จำนวนเงินรวมของทุกใบแจ้งยอดชำระไม่ตรงกัน กรุณาทำรายการใหม่อีกครั้ง",
                "Error, the total amount of Invoices"
            ),
            "messageStatus" => "fail"
        );
    }

    //time out
    $timeOut = date("Y-m-d H:i:s");
    $sql = "DELETE FROM $PAYMENT.lock WHERE timeOut < '$timeOut' ";
    $his->run_sql($sql);

    //check PAYMENT.lock
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){
            $invoiceId = $v['invoiceId'];
            $sql = "SELECT invoiceId
                    FROM $PAYMENT.lock
                    WHERE invoiceId = '$invoiceId' 
                    LIMIT 1";
            $lock = $his->run_sql_return_array1d($sql);
            if(!empty($lock)){
                return array(
                    "messageCode" => 20000,
                    "messageDescription" => array(
                        "ใบแจ้งยอดชำระ $invoiceId กำลังดำเนินการทำธุระกรรมอยู่ในขณะนี้ กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                        "Error, invoice $invoiceId in progress"
                    ),
                    "messageStatus" => "fail"
                );
            }
        }
    }

    //gen verifyRefID
    $datetime = date("YmdHis");
    $verifyRefID = str_pad($hn, '0', 7).substr($datetime, 1, strlen($datetime));
    $reference2 = $citizenId;

    //lock invoiceId
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){
            $invoiceId = $v['invoiceId'];
            $timeOut = date("Y-m-d H:i:s", time() + (60 * 3));
            $sql = "REPLACE INTO $PAYMENT.lock (invoiceId, verifyRefID) VALUES ('$invoiceId', '$verifyRefID');";
            $his->run_sql($sql);
            $sql = "UPDATE $PAYMENT.lock SET timeOut = '$timeOut' 
                    WHERE invoiceId = '$invoiceId' && verifyRefID =  '$verifyRefID' 
                    LIMIT 1";
            $his->run_sql($sql);
        }
    }
    $his->close_sql($connnect);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "verifyStatus" => true,
        "verifyRefID" => $verifyRefID,
        "reference2" => $reference2,
        "netAmount" => ($total - $roundOff),
        "roundOff" => $roundOff
    );

}


