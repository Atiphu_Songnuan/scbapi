<?php
/*
    WS007-5-Set Payment
    Update 2019-09-19
*/
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if (isset($hn)) {
    
    $rfu = new restful();
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $hospitalNumber = $post['hospitalNumber'];
    $citizenId = $post['citizenId'];
    $jsonData = json_encode($post);
    $verifyRefID = $post['verifyRefID'];
    $invoiceList = $post['invoiceList'];
    $receiptDetailList = array();

    $sql = "INSERT INTO $PAYMENT.payment (hospitalNumber, jsonData, verifyRefID, datetime) 
            VALUES ('$hospitalNumber', '$jsonData', '$verifyRefID', NOW());";
    $his->run_sql($sql);

    //bill
    $invoice_group = array();
    foreach ($invoiceList as $v) {
        $invoice_group[] = $v['invoiceId'];
    }

    $invoice_group = join(",", $invoice_group);
    $sql = "SELECT id, billName, amountInRight, amountOverRight, roundOff, performStatus
            FROM $PAYMENT.bill
            WHERE invoice_id IN (SELECT id 
                         FROM $PAYMENT.invoice
                         WHERE invoiceId IN ($invoice_group)
                        )";
    $bills = $his->run_sql_return_array($sql);
    $billList = array();
    $seq = 1;
    $receiptTotalAmountInRight = 0;
    $receiptTtotalAmountOverRight = 0;
    $receiptTotalAmount = 0;
    foreach ($bills as $v) {
        $billList[] = array(
            "billSequenceNo" => "$seq",
            "billName" => $v['billName'],
            "amountInRight" => floatval($v['amountInRight']),
            "amountOverRight" => floatval($v['amountOverRight']),  
        );
        $seq++;
        $receiptTotalAmountInRight += floatval($v['amountInRight']);
        $receiptTtotalAmountOverRight += floatval($v['amountOverRight']);
        $receiptTotalAmount += floatval($v['amountOverRight']);
        $receiptRoundOff += floatval($v['roundOff']);
    }
    $his->close_sql($connnect);


    // !!!!! ระวังตรงนี้สำคัญที่สุด !!!!! //

    

    // !!!!!!!!!!!!!!! //*/

    $sql = "SELECT GROUP_CONCAT(DISTINCT remark) AS remark, GROUP_CONCAT(DISTINCT eligible) AS eligible
            FROM $PAYMENT.invoice
            WHERE invoiceId IN ($invoice_group)";
    $receipt = $his->run_sql_return_array1d($sql);

    //info user
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HIS']);
    $Mydata = $isProduction? 'Mydata':'DATATEST';
    $sql = "SELECT A.pname, A.psur
            FROM $Mydata.Medrec as A
            WHERE A.hn ='$hn'
            LIMIT 1";
    $data = $his->run_sql_return_array1d($sql);
    $his->close_sql($connnect);
    $name =  $data['pname'].' '.$data['psur'];

    $result = array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "hospitalNumber" => $post['hospitalNumber'],
        "name" => $name,
        "receiptNo" => $receiptNo,
        "receiptDateTime" => $receiptDateTime,
        "verifyRefID" => $verifyRefID,
        "receiptDetailList" => $receiptDetailList,
        "receiptTotalAmountInRight" => $receiptTotalAmountInRight,
        "receiptTtotalAmountOverRight" => $receiptTtotalAmountOverRight,
        "receiptTotalAmount" => $receiptTotalAmount,
        "receiptNetAmount" => ($receiptTotalAmount - $receiptRoundOff),
        "receiptRoundOff" => $receiptRoundOff,
        "remark" => $receipt['remark'],
        "eligible" => $receipt['eligible']
    );

}