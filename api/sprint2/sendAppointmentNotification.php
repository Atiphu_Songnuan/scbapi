<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
// header('Content-Type: charset=utf-8');

set_time_limit(0);
ini_set('max_execution_time', 0); //0=NOLIMIT
ini_set("memory_limit", "-1");

$his = new class_mysql();
$connnect = $his->connectSQL($CONFIGS['server']['HIS']);

// $setEncode = "SET names tis620";
// $c_sql_his->run_sql($setEncode);

$sql = "SELECT  hn as hospitalNumber,
                concat(date_ap,'|',code_ap) as appointmentId,
                comment as appointmentTitle,
                date_ap as appointmentDate,
                time_ap as appointmentTime,
                code_ap as clinicCode,
                unit_ap as clinicName,
                '' as location,
                '' as remark,
                'OPD' as appointmentType,
                '' as appointmentStatus,
                '' as wardCode,
                '' as wardName
        FROM Appoint
        WHERE date_ap >= curdate()+interval 3 day
        LIMIT 10";

$data = $his->run_sql_return_array($sql, MYSQLI_ASSOC);
$his->close_sql($connnect);

$data = array_map(function ($v) {
    return $v === "" ? null : $v;
}, $data);

$appoint_data = "";
foreach ($data as $k => $v) {
    $payload = array(
        "hospitalNumber" => $v["hospitalNumber"],
        "appointmentId" => $v["appointmentId"],
        "appointmentTitle" => $v["appointmentTitle"],
        "appointmentDate" => $v["appointmentDate"],
        "appointmentTime" => $v["appointmentTime"],
        "clinicCode" => $v["clinicCode"],
        "clinicName" => $v["clinicName"],
        "location" => $v["location"],
        "remark" => $v["remark"],
        "appointmentType" => $v["appointmentType"],
        "appointmentStatus" => $v["appointmentStatus"],
        "wardCode" => $v["wardCode"],
        "wardName" => $v["wardName"],
    );
    $appoint_data .= json_encode($payload).PHP_EOL;
}

// print_r("Data: " . $appoint_data);

$jsonFile = "appointment-" . date("Y-m-d") . ".json";

@file_put_contents(ROOTPATH . "/logs/sent/appointment/".$jsonFile, trim($appoint_data) . PHP_EOL);

// $scriptFile = __DIR__ . "/sendAppointmentNotification.py";
// sleep(3);

// $command = escapeshellcmd("python sendAppointmentNotification.py " .$jsonFile);
// // print_r($command);
// $output = shell_exec($command);
// var_dump($output);
// die;

$result = array(
    "messageCode" => 10000,
    "messageDescription" => "",
    "messageStatus" => "success",
);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/*function microtime_float(){
list($usec, $sec) = explode(" ", microtime());
return ((float)$usec + (float)$sec);
}

$time_start = microtime_float();
$count = count($appointmentList);
echo "count: $count\r\n";

// print_r($data);
if ($count != 0) {
//$api = "http://203.154.116.83/ws/rest/receiveAppointmentNotification";
$i = 0;
foreach ($appointmentList as $k => $v) {

// print_r($appointmentList);
//********** Send Data to URL by cURL **********

/*$cmd = "python ".__DIR__."/sendApppoNoti.php $v ".microtime_float();
$i++;
echo "[$i] $v[hospitalNumber] => Sending\r\n";
//echo "$cmd\r\n";
@exec($cmd);

/*$ch = curl_init($api);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute the POST request
$responseNotification = curl_exec($ch);
//close cURL resource
curl_close($ch);

$resArr = (array) json_decode($responseNotification, false);
foreach ($resArr as $k => $v) {
if ($k === "messageCode") {
$resArr[$k] = (int)$v;
}
}
$result = $resArr;

if ($result['messageCode'] === 604) {
$result = array(
"messageCode" => 20000,
"messageDescription" => "ยังไม่ได้ลงทะเบียน App",
"messageStatus" => "fail",
);
} else{
$result = array(
"messageCode" => 10000,
"messageDescription" => "",
"messageStatus" => "success",
);
}*/

/*}

$time_end = microtime_float();
$time = round($time_end - $time_start, 2);
echo "Process time $time seconds";
die;

} else {
$result = array(
"messageCode" => 20000,
"messageDescription" => "ไม่มีข้อมูลการนัดหมาย",
"messageStatus" => "fail",
);
}*/
