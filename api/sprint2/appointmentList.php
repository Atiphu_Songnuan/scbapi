<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
// header('Content-Type: charset=utf-8');
if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        // $cs1="SET character_set_connection=utf8";
        // $c_sql_his->run_sql($cs1) or die("Err: set connection");
        // $cs2="SET character_set_results=utf8";
        // $c_sql_his->run_sql($cs2) or die("Err: set results");
        // $cs3="SET character_set_client=utf8";
        // $c_sql_his->run_sql($cs3) or die("Err: set client");

        // $setEncode = "SET names tis620";
        // $c_sql_his->run_sql($setEncode);

        // $sql = "SELECT concat(date_ap,'|',code_ap) as appointmentId , comment as appointmentTitle , date_ap as appointmentDate , time_ap as appointmentTime ,comment as appointmentType , '' as patientType , code_ap as clinicCode , unit_ap as clinicName , '' as unitCode , '' as unitName , '' as locationCode , '' as locaiton ,'' as appointmentStatus ,'' as wardCode ,'' as wardName
        //             FROM        Mydata.Appoint
        //             WHERE       hn='$hn'
        //             AND         date_ap >= curdate()
        //         UNION
        //         SELECT '0' as appointmentId , '' as appointmentTitle , adm_d as appointmentDate , adm_t as appointmentTime ,'Admit' as appointmentType , 'IPD' as patientType , '' as clinicCode , '' as clinicName , '' as unitCode , '' as unitName , '' as locationCode , '' as locaiton ,'' as appointmentStatus , admward as wardCode ,B.n_ward as wardName
        //             FROM        Mydata.Ipd_adm as A
        //             LEFT JOIN   Mydata.Fward as B on A.admward=B.c_ward
        //             WHERE       hn='$hn'
        //             AND         adm_d > curdate()
        //         UNION
        //         SELECT '0' as appointmentId , '' as appointmentTitle , or_date as appointmentDate , or_time as appointmentTime ,'OR' as appointmentType , 'OR' as patientType , '' as clinicCode , '' as clinicName , '' as unitCode , '' as unitName , '' as locationCode , '' as locaiton ,'' as appointmentStatus , '' as wardCode ,'' as wardName
        //             FROM        OPD.App_regis
        //             WHERE       hn='$hn'
        //             AND         or_date > curdate()
        //         UNION
        //         SELECT '0' as appointmentId , '' as appointmentTitle , date_ex as appointmentDate , atime as appointmentTime , concat(x_type,':',part_exam)  as appointmentType , 'Dx' as patientType , 'O99' as clinicCode , 'X-Ray' as clinicName , '' as unitCode , '' as unitName , '' as locationCode , '' as locaiton ,'' as appointmentStatus , '' as wardCode ,'' as wardName
        //             FROM        XRAY.Xray as A
        //             LEFT JOIN   XRAY.Xdetail as B on A.recnum=B.recnum
        //             WHERE       hn='$hn'
        //             AND         date_ex > curdate()
        //         UNION
        //         SELECT '0' as appointmentId , '' as appointmentTitle , date_lab as appointmentDate , '' as appointmentTime ,  test_name  as appointmentType , 'Lx' as patientType , 'OA8' as clinicCode , 'LAB' as clinicName , '' as unitCode , '' as unitName , '' as locationCode , '' as locaiton ,'' as appointmentStatus , '' as wardCode ,'' as wardName
        //             FROM        LAB.RQ_Log
        //             WHERE       hn='$hn'
        //             AND         date_lab > curdate()";

        //Data Test
        // $sql = "SELECT  hn
        //         FROM    Mydata.Appoint
        //         WHERE   date_ap >= curdate()
        //         LIMIT 1";
        // $data = $c_sql_his->run_sql_return_array1d($sql);
        // $hn = $data['hn'];

        $sql = "SELECT  concat('$hn', '|', date_ap,'|',code_ap) as appointmentId ,
                        comment as appointmentTitle ,
                        date_ap as appointmentDate ,
                        time_ap as appointmentTime ,
                        'OPD' as appointmentType ,
                        '' as patientType ,
                        code_ap as clinicCode ,
                        unit_ap as clinicName ,
                        '' as unitCode ,
                        '' as unitName ,
                        code_ap as locationCode ,
                        unit_ap as location ,
                        '' as appointmentStatus ,
                        '' as wardCode ,
                        '' as wardName
                FROM    Appoint
                WHERE   hn='$hn'
                AND     date_ap >= curdate()";

        $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        if (isset($data)) {
            $appointmentList = array();
            if (count($data) != 0) {
                //Set Phone Number list
                // $testdata = array("074541728","0958567419");
                // $allMedicalWelfares = array();
                foreach ($data as $v) {
                    foreach ($v as $key => $value) {
                        if ($v[$key] == null) {
                            $v[$key] = "";
                        }
                    }
                    array_push($appointmentList, $v);
                    // array_push($appointmentList, array(
                    //     "appointmentId" => $v["appointmentId"],
                    //     "appointmentTitle" => $v["appointmentTitle"],
                    //     "appointmentDate" => $v["appointmentDate"],
                    //     "appointmentTime" => $v["appointmentTime"],
                    //     "appointmentType" => $v["appointmentType"],
                    //     "patientType" => $v["patientType"],
                    //     "clinicCode" => $v["clinicCode"],
                    //     "clinicName" => $v["clinicName"],
                    //     "unitCode" => $v["unitCode"],
                    //     "unitName" => $v["unitName"],
                    //     "locationCode" => $v["locationCode"],
                    //     "location" => $v["location"],
                    //     "appointmentStatus" => $v["appointmentStatus"],
                    //     "wardCode" => $v["wardCode"],
                    //     "wardName" => $v["wardName"],
                    // ));
                }

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "appointmentList" => $appointmentList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีข้อมูลนัดหมาย",
                    "messageStatus" => "fail",
                    "appointmentList" => $appointmentList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}
