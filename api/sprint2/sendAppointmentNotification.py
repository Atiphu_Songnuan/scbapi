import os
import sys
import json
import base64
import urllib.request
from multiprocessing import Pool

def sendAppointmentNotification(value):

    url = "http://203.154.116.83/ws/rest/receiveAppointmentNotification"
    headers = {
        'User-Agent': 'Medicine PSU Server',
        'Content-Type': 'application/json'
    }
    try:
        request = urllib.request.Request(url = url, data = value, headers = headers)
        response = urllib.request.urlopen(request)
        res = response.read()
        print (value)
        print ("----- Response -----")
        print (res)
        print ("\n")
    #   if os.path.exists(savelog):
    #       with open(savelog, 'a') as file:
    #           file.write(res + ""\n\n"")

    except urllib.request.URLError as e:
        print ("Error: " + e.reason + " (" + value + ")")

jsonFile = sys.argv[1]
f = open(jsonFile)
contents =f.read()
print(contents)

JSON_DATA = '../logs/appointment.json'
WORKER = 10

pool = Pool(processes=WORKER)
with open(JSON_DATA) as f:
    for value in f:
        pool.apply_async(sendAppointmentNotification, [value])
    pool.close()
    pool.join()
