<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language", "appointmentType", "appointmentId")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        $splitAppointID = explode("|", $post['appointmentId']);
        $date_ap = $splitAppointID[1];
        $code_ap = $splitAppointID[2];

        // print_r($hn);
        //Data Test
        // $sql = "SELECT  hn
        // FROM    Mydata.Appoint
        // WHERE   date_ap >= curdate()
        // LIMIT 1";

        // $data = $c_sql_his->run_sql_return_array1d($sql);
        // $hn = $data['hn'];

        $sql = "  SELECT  concat('$hn', '|', date_ap,'|',code_ap)  as appointmentId,
                        comment as appointmentTitle,
                        date_ap as appointmentDate,
                        time_ap as appointmentTime,
                        'OPD' as appointmentType,
                        '' as patientType,
                        doct_reg as doctor,
                        code_ap as clinicCode,
                        unit_ap as clinicName,
                        '' as wardCode,
                        '' as wardName,
                        '' as treatmentType,
                        code_ap as locationCode,
                        unit_ap as location,
                        '' as preparationInfo,
                        '' as remark,
                        '' as appointmentStatus,
                        '' as journeyType,
                        '' as medicalWelfares,
                        '' as moreDetail
                FROM    Appoint
                WHERE   hn = '$hn'
                AND     date_ap = '$date_ap'
                AND     code_ap = '$code_ap'";

        $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        // print_r($data);

        if (isset($data)) {
            $opdAppointmentDetail = array();
            $ipdAppointmentDetail = array();
            if (count($data) != 0) {

                //Check value is NULL
                foreach ($data as $k => $v) {
                    if ($data[$k] == null) {
                        $data[$k] = "";
                    }
                }

                //Add new data to array
                array_push($opdAppointmentDetail, $data);

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "OPDList" => $opdAppointmentDetail,
                    "IPDList" => $ipdAppointmentDetail,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีข้อมูลรายละเอียดของนัดหมาย",
                    "messageStatus" => "fail",
                    "OPDList" => $opdAppointmentDetail,
                    "IPDList" => $ipdAppointmentDetail,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}
