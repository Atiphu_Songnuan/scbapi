<?php
/*  =============================================
Name : Pratomrerk
Email : Pratomrerk@gmail.com
v.4.4 29/03/2019
============================================= */
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

class class_mysql
{

    private $_connect;
    public $debug = false;

    public function connectSQL($conf)
    {
        $this->_connect = @new mysqli($conf['host'], $conf['user'], $conf['passwd'], $conf['dbname']);
        if ($this->_connect->connect_errno) {
            die("{\"error\":\"connect_error\"}");
        }
        $this->_connect->set_charset("UTF8");
        return $this->_connect;
    }

    public function run_sql($sql)
    {
        $this->_connect->query($sql) or die($this->onError($sql));
    }

    public function run_sql_return_array($sql, $resulttype = MYSQLI_BOTH)
    {
        $result = $this->_connect->query($sql) or die($this->onError($sql));
        $data = array();
        while ($array = $result->fetch_array($resulttype)) {
            $data[] = $array;
        }
        return $data;
    }

    public function run_sql_return_array1d($sql, $resulttype = MYSQLI_BOTH)
    {
        $result = $this->_connect->query($sql) or die($this->onError($sql));
        $data = $result->fetch_array($resulttype);
        $result->close();
        return is_null($data) ? array() : $data;
    }

    public function run_sql_return_num($sql)
    {
        $result = $this->_connect->query($sql) or die($this->onError($sql));
        $data = $result->num_rows;
        $result->close();
        return is_null($data) ? array() : $data;
    }

    public function insert_id()
    {
        return $this->_connect->insert_id;
    }

    public function close_sql($connect)
    {
        $connect->close();
    }

    private function onError($sql)
    {
        header("Location: /404");
        die;
    }

}
