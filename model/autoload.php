<?php
    if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
    spl_autoload_register(function($class) {
        $class = str_replace('class_', '', $class);
        $file = __DIR__ .'/'. $class . '.php';
        if(file_exists($file)) require_once($file);
    });
?>