<?php
/*  =============================================
      	Name : Pratomrerk
		Email : Pratomrerk@gmail.com
		v.1.0 (build 100919)
		Ref: https://medium.com/@phayao/แนวออกแบบ-restful-api-วิธีปฏิบัตที่ดี-c320d806e30b
    ============================================= */
	if(!defined('SECURITY')){header("HTTP/1.1 404 Not Found");die;}

class restful{

	private $query = "";

	public function toSqlQuery($paras = array()){

		$this->query = isset($paras['query'])? $paras['query'] : $_SERVER['QUERY_STRING'];
		$index = isset($paras['index'])? $paras['index'] : 0;
		$method = isset($paras['method'])? $paras['method'] : $this->getMethod();
		$table = isset($paras['table'])? $paras['table'] : "";
		$field = isset($paras['field'])? $paras['field'] : "*";
		$data = isset($paras['data'])? $paras['data'] : json_decode(@file_get_contents('php://input'), true);
		$limit = isset($paras['limit'])? $paras['limit'] : 30;
		$limit_update = isset($paras['limit_update'])? $paras['limit_update'] : 1;
		$limit_delete = isset($paras['limit_delete'])? $paras['limit_delete'] : 1; 

		if(empty($table)){
			$point = $this->getEndpoint($index);
			$table = $point['key'];
			$root_value = $point['value'];
		}
		if(empty($table)) $this->return_status(400, true);
		

		switch ($method) {
			
			case 'PUT':
				$action = isset($paras['replace'])? 'REPLACE' : 'INSERT';
				$v = $this->getValues($data);
				$columns = $v['field'];
				$values = $v['values'];
				if(empty($values)) $this->return_status(400, true);
				$sql = "$action INTO $table\n($columns) VALUES ($values)";
				break;

			case 'POST':
			case 'DELETE':
			default:
		
				//Columns
				if(is_array($field)){
					$columns = $this->getValues($field, false)['values'];
				}else{
					$columns = $field;
				}
				
				//Where
				$where = "";
				$i = 0;
				if(is_array($field)){
					$value = $this->strValue($root_value);
					$where = $field[0]." = $value && ";
				}
				$resource = $this->getEndpoint($index);
				while(!empty($resource['key']) && !empty($resource['value'])){
					$value = $this->strValue($resource['value']);
					$where .= $resource['key']." = $value  && ";
					$i++;
					$resource = $this->getEndpoint($index + $i);
				}
				if(strlen($where)>= 3) $where = substr($where, 0, -3);
				if(empty($where)) $where = "1";

				//Or
				$like = $this->getFiltering();
				if(!empty($like)){
					$slike = "";
					foreach ($like as $k => $v) {
						$v = $this->strValue($v);
						$slike.= "$k = $v || ";
					}
					$where = $where == "1"? "":"$where && ";
					$slike = trim(substr($slike, 0, -3));
					$where .= "($slike)";
				}

				//Like
				if($columns != '.'){
					$search = $this->getSearching();
					if(!empty($search)){
						$where = $where == "1"? "":"$where && ";
						$search = $this->strValue($search);
						$where.= "MATCH($columns) AGAINST ($search) IN BOOLEAN MODE ";
					}
				}

				//Order
				$order = $this->getSorting();
				if(!empty($order)){
					$order_by = 'ORDER BY ';
					foreach ($order as $v) {
						$order_by.= "$v[0] $v[1], ";
					}
					$order_by = trim(substr($order_by, 0, -2));
					$order_by.= "\n";
				}

				//Limit
				$page = $this->getPagination();
				$page = $page * $limit;


				switch ($method) {
 
					case 'POST':
						$set = '';
						foreach ($data as $k => $v) {
							$v = $this->strValue($v);
							$set.= "$k = $v, ";
						}
						if(empty($set)) $this->return_status(400, true);
						$set = trim(substr($set, 0, -2));
						$sql = "UPDATE $table\nSET $set\nWHERE $where\nLIMIT $limit_update"; 
						break;

					case 'DELETE':
						$sql = "DELETE FROM $table\nWHERE $where\nLIMIT $limit_delete";
						break;
					
					default: //GET
						$sql = "SELECT $columns\nFROM $table\nWHERE $where\n".$order_by."LIMIT";
						$sql.= ($page == 0 || $limit == 1)? " $limit":" $page,$limit";
						break;
				}

				break;
		}

		return "$sql;";
	}

	private $method_used = array('GET','POST', 'PUT', 'DELETE');

	public function getMethod(){
		$method = $_SERVER['REQUEST_METHOD'] == 'POST'? 'POST':'GET';
		$method = isset($_SERVER['HTTP_X_REST_METHOD'])? strtoupper($_SERVER['HTTP_X_REST_METHOD']):$method;
		foreach ($this->method_used as $v) {
			if($method == $v){
				return $v;
			}
		}
		return $this->method_used[0];
	}

	public function isGet(){
		return $restful->getMethod() == 'GET'? true:false;
	}
	
	public function getEndpoint($index){
		$query = strlen($this->query) == 0? $_SERVER['QUERY_STRING']:$this->query;
		$arg = parse_url($query);
		$req = explode('/', $arg['path']);
		array_shift($req);
		$index = $index * 2;
		$key = isset($req[$index])? $req[$index]:"";
		$value = isset($req[$index+1])? $req[$index+1]:"";
		return array(
			'key' => $key,
			'value' => $value,
		);
	}

	public function return_status($code, $exit = false){
		http_response_code($code);
		if($exit) exit;
	}

	private function getParams(){
		$query = strlen($this->query) == 0? $_SERVER['QUERY_STRING']:$this->query;
		$arg = parse_url($query);
		$data = array();
		parse_str($arg['query'], $paras);
		foreach ($paras as $key => $value) {
			$data[$key] = trim($value);
		}
		return $data;
	}

	private function getValues($arr, $strValue = true){
		$field = array();
		$values = array();
		foreach ($arr as $key => $value) {
			$field[] = $key;
			$values[] = $strValue? $this->strValue($value) : $value;
		}
		return array(
			'field' => join(',', $field),
			'values' => join(',', $values)
		);
	}

	private function strValue($value){
		$value = is_numeric($value) && gettype($value) != 'string'? $value : "'$value'";
		if(strlen($value) > 0) $value = (substr($value, 0, 1) == '0' && strlen($value) > 1)? "'$value'" : $value;
		return $value;
	}

	public function getSorting(){
		$get = $this->getParams();
		$value = array();
		$sort = isset($get['sort'])? explode(',', $get['sort']):"";
		foreach ($sort as $v) {
			$tmp = explode('_', $v);
			$order = strtoupper($tmp[1]) == 'ASC'? 'ASC':'DESC';
			$value[] = array(trim($tmp[0]), $order);
		}
		return $value;
	}

	public function getFiltering(){
		$get = $this->getParams();
		unset($get['sort']);
		unset($get['search']);
		unset($get['page']);
		return $get;
	}

	public function getSearching(){
		$get = $this->getParams();
		return isset($get['search'])? $get['search']:"";
	}

	public function getPagination(){
		$get = $this->getParams();
		$page = isset($get['page'])? intval($get['page']):0;
		return $page < 0? 0:$page;
	}

}

?>
